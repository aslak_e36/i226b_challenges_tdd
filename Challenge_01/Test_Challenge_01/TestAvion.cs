﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Challenge_01;

namespace Test_Challenge_01
{
    [TestClass]
    public class TestAvion
    {
        [TestMethod]
        public void Decoller_AvionDecolle_Success()
        {
            string status = "Sur piste";
            Avion avion = new Avion(status, 5000, 200);
            avion.Decolle();
            string result = avion.Status;
            string resultExcpeted = "En vole";
            Assert.AreEqual(resultExcpeted, result);
        }

        [TestMethod]
        public void Atterrir_AvionAtterit_Success()
        {
            string status = "En vole";
            Avion avion = new Avion(status, 5000, 200);
            avion.Atterit();
            string result = avion.Status;
            string resultExcpeted = "Sur piste";
            Assert.AreEqual(resultExcpeted, result);

        }

        [TestMethod]
        [ExpectedException(typeof(AvionEnVoleException))]
        public void Decoller_AvionEntrainDeVoler_Fail()
        {
            string status = "En vole";
            Avion avion = new Avion(status, 5000, 200);
            avion.Decolle();
        }

        [TestMethod]
        [ExpectedException(typeof(AvionSurPisteException))]
        public void Atterrir_AvionPoserSurPiste_Fail()
        {
            string status = "Sur piste";
            Avion avion = new Avion(status, 5000, 200);
            avion.Atterit();
        }
    }
}
