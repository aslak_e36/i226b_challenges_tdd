﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Challenge_01
{
    public class Avion
    {
        private string status;
        private int distance, gas;


        public Avion(string status, int distance, int gas)
        {

        }

        public void Decolle()
        {

        }

        public void Atterit()
        {

        }

        public string Status
        {
            get => this.status; set => this.status = value;
        }
    }

    [Serializable]
    public class AvionSurPisteException : Exception
    {
        public AvionSurPisteException()
        {
        }

        public AvionSurPisteException(string message) : base(message)
        {
        }

        public AvionSurPisteException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected AvionSurPisteException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }

    [Serializable]
    public class AvionEnVoleException : Exception
    {
        public AvionEnVoleException()
        {
        }

        public AvionEnVoleException(string message) : base(message)
        {
        }

        public AvionEnVoleException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected AvionEnVoleException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
