﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Airport
{
    [TestClass]
    public class TestAirport
    {
        [TestMethod]
        public void Airport_FreeLanding_Success()
        {
            //given
            int landing = 12;
            bool free = true;
            bool actualresult;

            Airport airport = new Airport(landing, free);
            //then
            actualresult = airport.Free;

            //when
            Assert.IsTrue(actualresult);
        }
        [TestMethod]
        public void Airport_DangerOnLanding_Success()
        {
            int landing = 12;
            string danger = "";
            string expectedresult = "12 Panne éclairage";
            string actualresult;

            Airport airport = new Airport(landing, danger);
            //then
            actualresult = airport.ToString();

            //when
            Assert.AreEqual(expectedresult, actualresult);
        }
    }
}
