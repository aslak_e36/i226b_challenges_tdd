﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airport
{
    public class Airport
    {
        private int landing;
        private string danger;
        private bool free = false;
        private bool frees = false;

        public Airport(int landing, bool free)
        {
            this.landing = landing;
            this.free = free;
        }

        public Airport(int landing, string danger)
        {
            this.landing = landing;
            this.danger = danger;
            this.free = free;
        }

        public int Landing { get => landing; set => landing = value; }
        public bool Free { get => frees; }
        public string Danger { get => danger; set => danger = value; }

        public override string ToString()
        {
            return landing.ToString() + danger.ToString();
        }
    }
}
